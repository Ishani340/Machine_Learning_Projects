'''
Support vector machines - 
'''
## KNN SCIKIT LEARN ACCURACY TEST ##
import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np
style.use('ggplot')

class support_vector_machine:
    def __init__(self, visualization = True):
        # all methods within a class in init method runs other methods don't
    # when the class is called
        self.visualization = visualization
        self.colors = {1:'r',-1:'b'}
        if self.visualization:
            self.fig = plt.figure()
            self.ax = self.fig.add_subplot(1,1,1) # 1 by 1 grid plot 1

    def fit(self, data): # fit basically implies training the data
        self.data = data
        # ||w|| = [w,b]   # key value pairs when done with the optimization
                # choose the wlowest key value of w
        opt_dict = {}

        transforms = [[1,1], [-1,1], [-1,-1], [1,-1]] # these transforms will be applied to vector of w
                    # transforms are set to check every version of the vector possible
        all_data = []
        for yi in self.data:
            for featureset in self.data[yi]:
                for feature in featureset:
                    all_data.append(feature)

        self.max_feature_vale = max(all_data)
        self.min_feature_vale = min(all_data)
        all_data = None # just so that we are not holding all data in memory once the algorithm finishes running
            # support vectors yi(xi.w +b)=1
        step_sizes = [self.max_feature_vale*0.1,
                      self.max_feature_vale * 0.01,
                      ## point of expense
                      self.max_feature_vale * 0.001]
                # step sizes is a series of optimizations going from 10%
            # big steps to 1% step size to 0.1% step size depending on how precise one
                #wants to get
        ## extremely expensive
        b_range_multiple = 2  # b doesn't need to be as precise as w
        b_multiple = 5
        latest_optimum = self.max_feature_vale*10 # 1st place where we cutoff a major value
            # when w has same value for every point within the vector

        for step in step_sizes:
            w = np.array([latest_optimum, latest_optimum])
            optimized = False # will stay false until we have no more steps to take

            while not optimized:
                for b in np.arange(-1*(self.max_feature_vale*b_range_multiple),
                    # won't be giving b the same optimization treatment as w
                                   self.max_feature_vale*b_range_multiple, step*b_multiple):
                    for transformation in transforms:
                        w_t = w*transformation
                        found_option = True
                        # weakest link in SVM functionality
                        # yi(xi.w + b) >=1
                        for i in self.data:
                            for xi in self.data[i]:
                                yi = i
                                if not yi*(np.dot(w_t, xi)+b)>=1:
                                   found_option = False

                        if found_option:
                            opt_dict[np.linalg.norm(w_t)] = [w_t, b] # get magnitude of the vector
                if w[0]<0:
                    optimized = True
                    print('optimized a step')
                else:
                        # w = [5,5]
                        # step = 1
                        # w - step = [4, 4]
                    w = w - step
            norms = sorted([n for n in opt_dict])
                #||w|| : [w, b]
            opt_choice = opt_dict[norms[0]]
            self.w = opt_choice[0]
            self.b = opt_choice[1]

            latest_optimum = opt_choice[0][0]+step*2
        for i in self.data:
            for xi in self.data[i]:
                yi=i
                print(xi,':', yi*(np.dot(self.w, xi)+self.b))



        # w and b are the true crux of a support vector machine
    def predict(self, features):
        # sign(x.w + b) # sign can be + or - for +-1
        classification = np.sign(np.dot(np.array(features), self.w)+self.b)
        if classification!=0 and self.visualization:
            self.ax.scatter(features[0], features[1], s = 200, marker = '*', c = self.colors[classification])

        return classification
    def visualize(self):
        [[self.ax.scatter(x[0], x[1], s=100,color=self.colors[i]) for x in data_dict[i]] for i in data_dict]
        def hyperplane(x, w, b, v):
            return(-w[0]*x-b+v) / w[1]

        datarange = (self.min_feature_vale*0.9, self.max_feature_vale*1.1)
        hyp_x_min = datarange[0]
        hyp_x_max = datarange[1]

        # (w.x+b)=1  # positive support vector hyperplane
        psv1 = hyperplane(hyp_x_min, self.w,self.b,  1)
        psv2 = hyperplane(hyp_x_max, self.w, self.b, 1)
        self.ax.plot([hyp_x_min, hyp_x_max], [psv1,psv2], 'k')

        #(w.x+b) = -1
        nsv1 = hyperplane(hyp_x_min, self.w,self.b,  -1)
        nsv2 = hyperplane(hyp_x_max, self.w, self.b, -1)
        self.ax.plot([hyp_x_min, hyp_x_max], [nsv1, nsv2], 'k')

        #(w*x+b) = 0
        db1 = hyperplane(hyp_x_min, self.w,self.b,  0)
        db2 = hyperplane(hyp_x_max, self.w, self.b, 0)
        self.ax.plot([hyp_x_min, hyp_x_max], [db1,db2], 'y--')

        plt.show()


data_dict={-1:np.array([[1,7], [2,8], [3,8],]),
           1:np.array([[5,1], [6,-1], [7,3],])}


svm = support_vector_machine()
svm.fit(data=data_dict)
predict_us = [[0, 10], [1,3], [3,4], [3,5], [5,5], [5,6], [6,-5], [5,8], ]
for p in predict_us:
    svm.predict(p)
svm.visualize()



'''
import numpy as np
from sklearn import preprocessing, cross_validation, neighbors
from sklearn import svm
import pandas as pd

accuracies = []
for i in range(25):

    df = pd.read_csv('cancer_Data.csv')
        # we know that we have missing data represented by ?
    df.replace('?', -99999, inplace=True)
         # check for useless da # ID has no role whatsoever in determining the class
        # remove ID for Now
    #df.drop(['id'], 1, inplace=True)

    X = np.array(df.drop(['class'], 1))        # for features
    y = np.array(df['class'])        # for labels

    X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.2)
    # define the classifier

    #clf=neighbors.KNeighborsClassifier(g)
    clf = svm.SVC()
    clf.fit(X_train, y_train)

    ## can test it immediately
    accuracy = clf.score(X_test, y_test)
    print(accuracy)
        # Including the ID column reduces the accuracy drastically
        # now we have trained this classifier and it's now eligible for making predictions
        # In most cases, if we deal with huge amounts of data, we pickle the classifier

    example_measures = np.array([[4,2,1,1,1,2,3,2,1],[4,2,1,2,2,2,3,2,1] ]) # an example record minus ID and minus class
            # make sure this set of example attributes does not exist
    example_measures = example_measures.reshape(len(example_measures), -1)

    prediction = clf.predict(example_measures)

    print(prediction) ## PREDICTS CLASS OF 2 - BENIGN
 #   accuracies.append(accuracy)
#print(sum(accuracies)/len(accuracies))

## 97.14% accuracy in KNN scikit learn

 96.94% from the algorithm we wrote


'''