from statistics import mean
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style

xs = np.array([1,2,3,4,5,6], dtype = np.float64)
ys = np.array([5,4,6,5,6,7], dtype = np.float64)
'''
data is a numpy array not a python list 
define a function that would return the best fit slope '''
style.use('fivethirtyeight')
def bestslope_intercept(xs, ys):
    m = (((mean(xs)*mean(ys))-mean(xs*ys)) /
          ((mean(xs)**2)-mean(xs**2)))
    b = mean(ys) - m*mean(xs)
    return m,b
#print(m, b)
'''
now what if we want to create a line that fits the data
m = 0.4285 and b = 4.0 '''
 # implies we are using the same Xs as one defined above
        # identical to for x in xs:
                        #regression_line.append(m*x+b)
## say we can now make a prediction if we want
def squared_error(ys_origin, ys_line): # difference between Ys original and the Ys line
    return sum((ys_line - ys_origin )**2)  # this is squared error for entire line

def coefficient_Determination(ys_origin, ys_line):
    y_mean_line = [mean(ys_origin) for y in ys_origin]
    squared_error_regr = squared_error(ys_origin, ys_line)
    square_error_y_mean = squared_error(ys_origin, y_mean_line)
    return 1-(squared_error_regr/square_error_y_mean)

m, b = bestslope_intercept(xs, ys)
regression_line = [(m*x)+b for x in xs]
predict_x = 8  # we'll predict y where x is 8
predict_y = (m * predict_x) + b

r_squared = coefficient_Determination(ys, regression_line)
print(r_squared)
        # 0.58 value of r-squared
plt.scatter(xs, ys)
plt.scatter(predict_x, predict_y, color = 'g')
plt.plot(xs, regression_line)
plt.show()
                #### HOW ACCURATE IS OUR BEST FIT LINE####
                ## R - Squared = coefficient of determination
                    # tells how good of a fit the best fit line is
                # will create a new function which calculates r-squared


'''
gives slope as 0.4285 
also need to calculate the y-intercept 
'''

#plt.scatter(xs,ys)
#plt.show()