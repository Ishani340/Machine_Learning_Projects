## KNN SCIKIT LEARN ACCURACY TEST ##

import numpy as np
from sklearn import preprocessing, cross_validation, neighbors
import pandas as pd

accuracies = []
for i in range(25):

    df = pd.read_csv('cancer_Data.csv')
        # we know that we have missing data represented by ?
    df.replace('?', -99999, inplace=True)
        # check for useless da # ID has no role whatsoever in determining the class
        # remove ID for Now
    df.drop(['id'], 1, inplace=True)

    X = np.array(df.drop(['class'], 1))        # for features
    y = np.array(df['class'])        # for labels

    X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.2)
    # define the classifier

    clf=neighbors.KNeighborsClassifier(g)
    clf.fit(X_train, y_train)

    ## can test it immediately
    accuracy = clf.score(X_test, y_test)
    #print(accuracy)
        # Including the ID column reduces the accuracy drastically
        # now we have trained this classifier and it's now eligible for making predictions
        # In most cases, if we deal with huge amounts of data, we pickle the classifier

    #example_measures = np.array([[4,2,1,1,1,2,3,2,1],[4,2,1,2,2,2,3,2,1] ]) # an example record minus ID and minus class
            # make sure this set of example attributes does not exist
    #example_measures = example_measures.reshape(len(example_measures), -1)

    #prediction = clf.predict(example_measures)

    #print(prediction) ## PREDICTS CLASS OF 2 - BENIGN
    accuracies.append(accuracy)
print(sum(accuracies)/len(accuracies))

## 97.14% accuracy in KNN scikit learn

## 96.94% from the algorithm we wrote
