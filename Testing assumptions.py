from statistics import mean
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
import random

xs = np.array([1,2,3,4,5,6], dtype = np.float64)
ys = np.array([5,4,6,5,6,7], dtype = np.float64)
style.use('fivethirtyeigh t')

def create_dataset(hm, variance, step=2, correlation = False):
    val = 1
    ys = []
    for i in range(hm):
        y = val + random.randrange(-variance, variance)
        ys.append(y)
        if correlation and correlation == 'pos':
            val+=step
        elif correlation and correlation =='neg':
            val-=step
    xs = [i for i in range(len(ys))]


    return np.array(xs, dtype=np.float64), np.array(ys, dtype =np.float64)


def bestslope_intercept(xs, ys):
    m = (((mean(xs)*mean(ys))-mean(xs*ys)) /
          ((mean(xs)**2)-mean(xs**2)))
    b = mean(ys) - m*mean(xs)
    return m,b


def squared_error(ys_origin, ys_line): # difference between Ys original and the Ys line
    return sum((ys_line - ys_origin )**2)  # this is squared error for entire line

def coefficient_Determination(ys_origin, ys_line):
    y_mean_line = [mean(ys_origin) for y in ys_origin]
    squared_error_regr = squared_error(ys_origin, ys_line)
    square_error_y_mean = squared_error(ys_origin, y_mean_line)
    return 1-(squared_error_regr/square_error_y_mean)


xs, ys = create_dataset(40, 1000, 2, correlation = 'pos')
m, b = bestslope_intercept(xs, ys)
regression_line = [(m*x)+b for x in xs]
predict_x = 8  # we'll predict y where x is 8
predict_y = (m * predict_x) + b

r_squared = coefficient_Determination(ys, regression_line)
print(r_squared)

plt.scatter(xs, ys)
plt.scatter(predict_x, predict_y, s = 100, color = 'g')
plt.plot(xs, regression_line)
plt.show()
