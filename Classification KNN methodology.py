'''
classification - create a model that best divides/separates our data
 Linear regression - create model that best fits our data
  nearest neighboors - check out the proximity of the concerned point with other groups
   
   most people use K-Nearest neighbors - If K =2, find 2 nearest ones 
   
   can have multiple groups spread across multi-dimensional model
    larger the data set, worse the KNN gets; SVM much more efficient
     There's never a point when u are actually training the algorithm; scaling is not so good
'''
'''
Breast cancer wisconsin - original data set - UCI ML repository
 Link: http://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/breast-cancer-wisconsin.data
'''
'''
import numpy as np
from sklearn import preprocessing, cross_validation, neighbors
import pandas as pd

df = pd.read_csv('cancer_Data.csv')
    # we know that we have missing data represented by ?
df.replace('?', -99999, inplace=True)
    # check for useless da # ID has no role whatsoever in determining the class
    # remove ID for Now
df.drop(['id'], 1, inplace=True)

X = np.array(df.drop(['class'], 1))        # for features
y = np.array(df['class'])        # for labels

X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.2)
# define the classifier

clf=neighbors.KNeighborsClassifier()
clf.fit(X_train, y_train)

## can test it immediately
accuracy = clf.score(X_test, y_test)
print(accuracy)
    # Including the ID column reduces the accuracy drastically
    # now we have trained this classifier and it's now eligible for making predictions
    # In most cases, if we deal with huge amounts of data, we pickle the classifier

example_measures = np.array([[4,2,1,1,1,2,3,2,1],[4,2,1,2,2,2,3,2,1] ]) # an example record minus ID and minus class
        # make sure this set of example attributes does not exist
example_measures = example_measures.reshape(len(example_measures), -1)
prediction = clf.predict(example_measures)

print(prediction) ## PREDICTS CLASS OF 2 - BENIGN
            # with 97.8 per cent accuracy
## closeness is measured by euclidean distance
from math import sqrt
plot1 = [1,3]
plot2 = [2,5]
euclidean_distance = sqrt((plot1[0]-plot2[0])**2+(plot1[1]-plot2[1])**2)
print(euclidean_distance)
'''
import numpy as np # has a builtin function for calc. euclidean distance
from math import sqrt
import matplotlib.pyplot as plt
import warnings
from matplotlib import  style
from collections import Counter
style.use('fivethirtyeight')
plot1 = [1,3]
plot2 = [2,5]

# euclidean_distance = sqrt((plot1[0]-plot2[0])**2+(plot1[1]-plot2[1])**2)
dataset = {'k':[[1,2], [2,3], [3,1]], 'r':[[6,5], [7,7], [8,6]]} # r is the class label
'''
this is a class of K- it is a list of lists - these are features - 2D features
these are features which correspond to class of K. 
'''
predict = [5,7] # this is the new point

'''for i in dataset:
    for ii in dataset[i]:
        plt.scatter(ii[0], ii[1], s = 100, color = i)
'''
# to build a one liner

    #[[plt.scatter(ii[0], ii[1], s=100, color = i) for ii in dataset[i]] for i in dataset]

#plt.scatter(new_features[0], new_features[1], s = 100)
#plt.show()

def k_nearest_neighbors(data, predict, k = 3):# data is training data
    if len(data)>= k: # if len of dictionary is greater or equal to the value of k that we would choose
        warnings.warn('K is set to value less than total voting groups!')

        # need to compare prediction point to all points in data set
        # lies the problem of KNN - high complexity
    distances = []
    for group in data: # for each class in data basically
        for features in data[group]: # iterating through [1,1] [1,2] so on
            #euclidean_distance = sqrt((plot1[0] - plot2[0]) ** 2 + (plot1[1] - plot2[1]) ** 2)
                    # not a fast calculation
            euclidean_distance = np.linalg.norm(np.array(features)-np.array(predict))
                    # this is faster

            distances.append([euclidean_distance, group]) # list of list
                # can sort the list # get top 3 distances
    votes = [i[1] for i in sorted(distances)[:k]]
    print(Counter(votes).most_common(1))
    vote_result = Counter(votes).most_common(1)[0][0] # most common gives array of list
        # first 0 gives the list most common
        # second 0 gives the most common distance

    return vote_result
result = k_nearest_neighbors(dataset, predict, k = 3)
print(result)
'''
result is 
[('r', 3)]
r 
return most voted thing was 'r'
 new data point is closest to the r group 
 so class of the data type is r type 
'''
[[plt.scatter(ii[0], ii[1], s=100, color = i) for ii in dataset[i]] for i in dataset]

plt.scatter(predict[0], predict[1], s = 100, color = result)
plt.show()
        # WANT TO COMPARE THIS SCI KIT LEARN'S algorithm performance for KNN

        # FOR BREAST CANCER DATA SET NEXT FILE
        # Will sci kit learn classifier do better than us 

























