import numpy as np  # has a builtin function for calc. euclidean distance
from math import sqrt
import matplotlib.pyplot as plt
import warnings
from matplotlib import style
from collections import Counter
import pandas as pd
import random

dataset = {'k': [[1, 2], [2, 3], [3, 1]], 'r': [[6, 5], [7, 7], [8, 6]]}  # r is the class label
predict = [5, 7]  # this is the new point
def k_nearest_neighbors(data, predict, k=3):  # data is training data
    if len(data) >= k:  # if len of dictionary is greater or equal to the value of k that we would choose
        warnings.warn('K is set to value less than total voting groups!')
    distances = []
    for group in data:  # for each class in data basically
        for features in data[group]:  # iterating through [1,1] [1,2] so on
            euclidean_distance = np.linalg.norm(np.array(features) - np.array(predict))
            distances.append([euclidean_distance, group])  # list of list

    votes = [i[1] for i in sorted(distances)[:k]]
    # confidence given by classifier
    #print(Counter(votes).most_common(1))
    vote_result = Counter(votes).most_common(1)[0][0]  # most common gives array of list
    confidence = Counter(votes).most_common(1)[0][1] / k
            # 1st element because how many
    #print(vote_result, confidence)
    return vote_result, confidence
# result = k_nearest_neighbors(dataset, predict, k=3)
# print(result)

#[[plt.scatter(ii[0], ii[1], s=100, color=i) for ii in dataset[i]] for i in dataset]

#plt.scatter(predict[0], predict[1], s=100, color=result)
#plt.show()
accuracies = []
for i in range(25):

    df = pd.read_csv("cancer_Data.csv")
    df.replace('?', -99999, inplace=True)
    df.drop(['id'], 1, inplace=True)
    full_data = df.astype(float).values.tolist()
            # cus everything in the dataset needs to be in float

    #print(full_data[:10]) # we can now shuffle data since we have converted it to
                        # a list of list in the full data statement
    random.shuffle(full_data)

    #print(full_data[:5])

    test_size = 0.4
    train_set = {2:[], 4:[]}
    test_set = {2:[], 4:[]}
    train_data = full_data[:-int(test_size*len(full_data))]
                        # using test size to slice some training data from full data

    test_data = full_data[-int(test_size*len(full_data)):]

    for i in train_data:
        train_set[i[-1]].append(i[:-1]) # i[-1] is last column is list either 2 or 4
                                # that's how we are identifying between 2 or 4
                        # append - appending lists into this list - elements upto the last column
    for i in test_data:
        test_set[i[-1]].append(i[:-1])

    correct = 0
    total = 0
    for group in test_set:
        for data in test_set[group]:
            vote, confidence = k_nearest_neighbors(train_set, data, k = 5)
            if group == vote:
                correct+=1

            total+=1

    #print('Accuracy:', correct/total )
    accuracies.append(correct/total)
print(sum(accuracies)/len(accuracies))
            # This implies that atleast 5 tests where we average
                    # 96.7 per cent accuracy

































